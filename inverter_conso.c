/**
 * Inverter production container
 *
 * @author Pierre HUBERT
 */

#include "inverter_conso.h"

static inverter_conso_update_t inverter_update_data;

void inverter_conso_init(){
	pthread_mutex_init(&inverter_update_data.mutex, NULL);
	pthread_cond_init(&inverter_update_data.condition, NULL);
}

inverter_conso_update_t *inverter_conso_get(){
	return &inverter_update_data;
}

void inverter_conso_update(const inverter_conso_t conso){
	pthread_mutex_lock(&inverter_update_data.mutex);
	inverter_update_data.conso = conso;
	pthread_mutex_unlock(&inverter_update_data.mutex);
	
	pthread_cond_broadcast(&inverter_update_data.condition);
}

bool inverter_conso_validate(const inverter_conso_t *conso){
	return conso->valid;
}