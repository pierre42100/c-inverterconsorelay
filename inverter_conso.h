/**
 * Inverter consuption container
 *
 * @author Pierre HUBERT
 */

#include <pthread.h>

#include "global.h"

typedef struct inverter_conso_t {
	int power_phase_1;
	int power_phase_2;
	int power_phase_3;
	bool valid;
} inverter_conso_t;

typedef struct inverter_conso_update_t {
	inverter_conso_t conso;
	pthread_mutex_t mutex;
	pthread_cond_t condition;
} inverter_conso_update_t;


/**
 * Initialize inverter consumption transmission
 */
void inverter_conso_init();

/**
 * Get inverter update information
 */
inverter_conso_update_t *inverter_conso_get();

/**
 * Update inverter consuption information
 *
 * @param conso New inverter consumption information
 */
void inverter_conso_update(const inverter_conso_t conso);

/**
 * Check out whether inverter consumption information hold
 * by an inverter_conso_t object are valid or not
 *
 * @param *conso Object to check
 * @return true if the object is valid / FALSE else 
 */
bool inverter_conso_validate(const inverter_conso_t *conso);