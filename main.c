/**
 * Inverter consumption relay
 *
 * Main project file
 *
 * @author Pierre HUBERT
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

#include "pull_thread.h"
#include "push_thread.h"
#include "inverter_conso.h"
#include "global.h"

//Catch term signals to close correctly sockets
static volatile sig_atomic_t keep_running = 1;
static void sig_int(int c);

int main(int argc, char **argv){

	LOG_VERBOSE("Fronius inverter relay.\n(c) Pierre HUBERT 2018.\nLicensed under the MIT License.");

	//Inverter conso information
	LOG_VERBOSE("Initialize inverter consumption transition.");
	inverter_conso_init();

	//Avoid crash because of failed write
	signal (SIGPIPE, SIG_IGN);

	//Make sure all the sockets are closed before quitting the application
	signal (SIGINT, sig_int);
	signal (SIGTERM, sig_int);

	//Start pull thread
	LOG_VERBOSE("Starting pull thread.");
	if(!pull_thread_start())
		LOG_FATAL("Could not start pull thread!");
	LOG_VERBOSE("Pull thread started.");
    

	//Start push thread
	LOG_VERBOSE("Starting push thread");
	if(!push_thread_start())
		LOG_FATAL("Could not start push thread!");
	LOG_VERBOSE("Push thread started");

	//Wait loop
	while(keep_running == 1)
		sleep(100);

	LOG_VERBOSE("Ctrl+C pressed, quitting.");

	LOG_VERBOSE("Query stop pull thread.");
	pull_thread_stop();

	LOG_VERBOSE("Query stop push thread.");
	push_thread_stop();

	return EXIT_SUCCESS;
}

static void sig_int(int c){
	keep_running = 0;
}