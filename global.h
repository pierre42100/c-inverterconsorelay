/**
 * Global project definitions. Should be included in 
 * every peace of the project
 *
 * @author Pierre HUBERT
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "config.h"


#define LOG_VERBOSE(msg) {printf("Verbose: %s\n", msg);}
#define LOG_ERROR(msg) {fprintf(stderr, "Error: %s\n", msg);}
#define LOG_FATAL(msg) {fprintf(stderr, "Fatal: %s\n", msg); exit(EXIT_FAILURE);}

#define MALLOC_OR_FATAL(variable, size) {variable = malloc(size); if(variable == NULL) LOG_FATAL("Could not allocate memory!");}