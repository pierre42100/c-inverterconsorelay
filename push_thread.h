/**
 * Push thread methods
 *
 * @author Pierre HUBERT
 */

#include "global.h"

/**
 * Start push thread
 *
 * @return TRUE in case of success / FALSE else
 */
bool push_thread_start();

/**
 * Stop push thread
 */
void push_thread_stop();