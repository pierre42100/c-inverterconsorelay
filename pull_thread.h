/**
 * Pull thread functions
 *
 * @author Pierre HUBERT
 */

#include <pthread.h>

#include "global.h"

/**
 * Start pull refresh thread and wait for the listen socket
 * to be successfully started 
 *
 * @return TRUE in case of success / FALSE else
 */
bool pull_thread_start();

/**
 * Query pull thread to be stopped 
 */
void pull_thread_stop();