#include <sys/socket.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#include "pull_thread.h"
#include "inverter_conso.h"

static pthread_t thread;
static int socket_srv;

static void *pull_thread_loop(void* data);

bool pull_thread_start(){

	struct sockaddr_in server;

	socket_srv = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_srv == -1)
		LOG_FATAL("Could not create socket!");

	//Bind socket to port
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PULL_LISTEN_PORT);
	if(bind(socket_srv, (struct sockaddr*)&server, sizeof(server)) < 0)
		LOG_FATAL("Could not bind pull socket to port!");

	//Listen on port
	listen(socket_srv, 3);

	//Start new thread to wait for connection
	int *socket_srv_ptr;
	MALLOC_OR_FATAL(socket_srv_ptr, sizeof(int));
	*socket_srv_ptr = socket_srv;
    if(pthread_create(&thread, NULL, pull_thread_loop, socket_srv_ptr) < 0)
        LOG_FATAL("Could not create pull thread loop!");
    
    
	return true;
}

static pthread_t *pull_thread_get_main(){
	return &thread;
}

/**
 * Search and return a numeric value in the JSON response
 *
 * @param const char * string The target string to perform research into
 * @param const char * name   The name of the research to make 
 * @param bool * valid If fails to get value, this variable will be set to false
 * @return int -1 in case of failure, value else
 */
static int find_numeric_value(const char *src, const char *name, bool *valid){

	//Search the occurrence in the string
	char* pos_begin = strstr(src, name);
	if(pos_begin == NULL){
		*valid = false;
		return -1;
	}

	//Remove value name
	pos_begin = strchr(pos_begin, ':');
	if(pos_begin == NULL){
		*valid = false;
		return -1;
	}

	//Search the string value
	char *pos_end = strstr(pos_begin, ",");

	if(pos_end == NULL){
		*valid = false;
		return -1;
	}

	//Extract value
	while(*pos_begin < '0' || *pos_begin > '9'){
		pos_begin++;

		if(pos_begin == pos_end){
			*valid = false;
			return -1;
		}
	}

	//Check if the value is negative
	bool negative  = *(pos_begin-1) == '-';

	//Check string length
	if(strlen(pos_begin) - strlen(pos_end) > 30){
		*valid = false;
		return -1;
	}

	//Copy value to new string
	char string[31];
	strncpy(string, pos_begin, pos_end-pos_begin);
	string[31] = '\0';

	//Remove potential point
	char *point_pos = strchr(string, '.');
	if(point_pos != NULL)
		*point_pos = '\0';
	
	return atoi(string) * (negative ? -1 : 1);

}

/**
 * Main thread loop
 *
 * @param data Pointer on socket numer
 */
static void *pull_thread_loop(void* data){

	int socket_srv = *((int*)data);
	free(data);

	LOG_VERBOSE("Main pull thread loop started.");

	//Accept incoming connections
	int new_socket;
	struct sockaddr_in client;
	int c = sizeof(struct sockaddr_in);
	while((new_socket = accept(socket_srv, (struct sockaddr *)&client, (socklen_t*)&c))) {

		LOG_VERBOSE("New incoming push connection accepted.");

		//Read stream
		char string[5000];
		read(new_socket, string, 5000);
		string[5000] =  '\0';

		//Parse received string
		inverter_conso_t conso;
		conso.valid = true;
		conso.power_phase_1 = find_numeric_value(string, "PowerReal_P_Phase_1", &conso.valid);
		conso.power_phase_2 = find_numeric_value(string, "PowerReal_P_Phase_2", &conso.valid);
		conso.power_phase_3 = find_numeric_value(string, "PowerReal_P_Phase_3", &conso.valid);

		//Write response to server
		int response_code;
		char response_content[100];
		if(inverter_conso_validate(&conso)){
			response_code = 200;
			sprintf(response_content, "Thank you for your response!");
		}
		else {
			response_code = 400;
			sprintf(response_content, "Invalid data!");
		}


		//Mimic real HTTP server
		char *response;
		MALLOC_OR_FATAL(response, 1000*sizeof(char));
		sprintf(response, "HTTP/1.1 %s\n", response_code == 200 ? "200 OK" : "400 Bad Request");
		sprintf(strchr(response, '\0'), "Connection: Close\n");
		sprintf(strchr(response, '\0'), "Content-Length: %ld\n\n", strlen(response_content));

		sprintf(strchr(response, '\0'), "%s", response_content);

		write(new_socket, response, strlen(response));


		//Terminate socket connection
		free(response);
		close(new_socket);
		
		//Notify other threads about consumption update (if possible)
		if(inverter_conso_validate(&conso))
			inverter_conso_update(conso);
	}


	return NULL;
}

void pull_thread_stop(){
	pthread_cancel(*pull_thread_get_main());
	close(socket_srv); // Close server socket
}