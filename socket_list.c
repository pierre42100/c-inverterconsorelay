
#include "global.h"
#include "socket_list.h"

typedef struct socket_entry_t {
	int socket;
	struct socket_entry_t *previous;
} socket_entry_t;

static socket_entry_t *last_entry = NULL;

void socket_list_add(int socket){
	
	socket_entry_t *el;
	MALLOC_OR_FATAL(el, sizeof(socket_entry_t));

	el->previous = last_entry;
	el->socket = socket;

	last_entry = el;
}

void socket_list_remove(int socket){

	//Process the list of entries
	socket_entry_t *previous_entry = NULL;
	socket_entry_t *curr_entry = last_entry;

	while(curr_entry != NULL){
		if(curr_entry->socket == socket){
			if(previous_entry != NULL)
				previous_entry->previous = curr_entry->previous;
			free(curr_entry);
			return;
		}

		previous_entry = curr_entry;
		curr_entry = curr_entry->previous;
	}

}

void socket_list_forall(socket_list_callback_t cb){

	socket_entry_t *curr_entry = last_entry;

	while(curr_entry != NULL){
		cb(curr_entry->socket);
		curr_entry = curr_entry->previous;
	}

}