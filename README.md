# Inverter consumption relay
This project is designed to make a relay between an Inverter Push Service and an unlimited number of pull clients. This project was written in C.

## How it works
The project make the machine listen on two ports:
* The port 3105 is the port where the Fronius inverter push regulary data (I recommend you to choose the lowest refresh interval - for me it is 10 seconds). Note : the project mimic an HTTP server on this port in order to make the Fronius inverter belives this project is a real web server.
* The port 3106 is the port where TCP clients connects. The sockets connections stay alive until the client closes it or this relay is stopped. Each time the relay receive new data from the inverter, it sends all the new data to all the sockets, in a JSON-encoded format.

## Adjust listen ports
Please have a look at config.h

## Adjust received data
Please update correctly the following file to make the application fit your needs :
* inverter_conso.h
* pull_thread.c
* push_thread.c

## Dependencies
This project is made to run on Linux OSs only, and is originally targetting a Raspberry PI. It does not make use of any libray apart the standard C library and the linux system libraries.

## Building
```sh
make
```

## Installing as a service
You can run the following commands to install the system as a service:
```sh
sudo cp inverterconsorelay.service /etc/systemd/system/
sudo systemctl start inverterconsorelay
sudo systemctl enable inverterconsorelay
```

## Configure a Fronius Inverter
This screenshot present a sample configuration of a Fronius inverter system :
![Sample configuration screenshot](SampleConnectionSettings.png)


## License
This programm is licensed under the MIT Licence

## Author
Pierre HUBERT 2018
