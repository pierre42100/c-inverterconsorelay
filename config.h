/**
 * Configuration file
 *
 * @author Pierre HUBERT
 */

/**
 * Pull server listen port
 */
#define PULL_LISTEN_PORT 3105

/**
 * Push listen port
 */
#define PUSH_LISTEN_PORT 3106