#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

#include "config.h"
#include "push_thread.h"
#include "inverter_conso.h"
#include "socket_list.h"

static pthread_t main_thread;
static int socket_srv;

static void *push_client_handler(void *data);
static void *push_client_main_socket_handler(void *data);

bool push_thread_start(){

	//Create server socket
	socket_srv = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_srv == -1)
		LOG_FATAL("Could not create pull main thread!");

	//Bind socket to port
	struct sockaddr_in server;
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PUSH_LISTEN_PORT);
	if(bind(socket_srv, (struct sockaddr *)&server, sizeof(server)) < 0)
		LOG_FATAL("Could not bind pull main socket!");

	//Listen to socket
	listen(socket_srv, 5);

	//Create main handler thread
	int* socket_srv_ptr;
	MALLOC_OR_FATAL(socket_srv_ptr, sizeof(int));
	*socket_srv_ptr = socket_srv;
	if(pthread_create(&main_thread, NULL, push_client_main_socket_handler, (void*)socket_srv_ptr) < 0)
		LOG_FATAL("Could not create push clients main thread!");

	return true;
}

static void *push_client_main_socket_handler(void *data){

	//Get server socket pointer
	int socket_srv = *(int*)data;
	free(data);

	//Accept all incoming connections (no protection)
	int new_socket = 0;
	struct sockaddr_in client;
	int c = sizeof(struct sockaddr_in);
	while( (new_socket = accept(socket_srv, (struct sockaddr *)&client, (socklen_t*)&c)) ) {

		LOG_VERBOSE("Accepted new connection.");

		//Add the socket to the list
		socket_list_add(new_socket);

		//Create new handler thread for the socket
		int *client_socket_ptr;
		MALLOC_OR_FATAL(client_socket_ptr, sizeof(int));
		*client_socket_ptr = new_socket;

		pthread_t client_thread;
		if(pthread_create(&client_thread, NULL, push_client_handler, (void*)client_socket_ptr) < 0)
			LOG_ERROR("Could not start a thread to handle client connection!");
	}

	return NULL;
}

static void *push_client_handler(void *data){

	//Get socket number
	int sock = *(int*)data;
	free(data);

	//Now we can wait for data to be available
	inverter_conso_update_t *conso_update = inverter_conso_get();

	while(true){
		pthread_mutex_lock(&conso_update->mutex);
		pthread_cond_wait(&conso_update->condition, &conso_update->mutex);

		//Create message to send through socket
		char message[1000];
		sprintf(message, 
			"{\"Phase1\": %d, \"Phase2\": %d, \"Phase3\": %d}", 
			conso_update->conso.power_phase_1,
			conso_update->conso.power_phase_2,
			conso_update->conso.power_phase_3
		);

		//Compute message length
		char message_length[6];
		sprintf(message_length, "%05ld", strlen(message));

		//Release mutex
		pthread_mutex_unlock(&conso_update->mutex);


		//Write to socket
		if(write(sock, message_length, strlen(message_length)) == -1 ||
			write(sock, message, strlen(message)) == -1){

			LOG_VERBOSE("Connection of one client closed!");
			socket_list_remove(sock);
			close(sock);
			break;

		}

		
	}

	return NULL;
}

static void socket_entry_callback(int socket){
	LOG_VERBOSE("Closing client socket");
	if(shutdown(socket, SHUT_RDWR) < 0)
		LOG_ERROR("Could not shutdown client socket!");
	
	if(close(socket) < 0)
		LOG_ERROR("Could not close a socket!");
}

void push_thread_stop(){
	//Stop to accept incoming connections
	pthread_cancel(main_thread);
	close(socket_srv);

	//Now we have to close all running connection
	socket_list_forall(&socket_entry_callback);
}