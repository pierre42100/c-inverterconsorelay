/**
 * Socket list management
 *
 * @author Pierre HUBERT
 */

/**
 * Callback function to do for all socket entries
 */
typedef void (*socket_list_callback_t) (int);

/**
 * Add a socket to the list
 *
 * @param socket The socket to add
 */
void socket_list_add(int socket);

/**
 * Remove a socket from the list
 */
void socket_list_remove(int socket);

/**
 * Process a function for the entire list of sockets
 */
void socket_list_forall(socket_list_callback_t cb);