CC=gcc
CFLAGS = -Wall -lpthread
DEPS = 
OBJ = main.o pull_thread.o inverter_conso.o push_thread.o socket_list.o

all: clean inverter_conso_relay

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

inverter_conso_relay: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

run: clean inverter_conso_relay
	./inverter_conso_relay

clean:
	rm -f *.o inverter_conso_relay

install: all
	cp ./inverter_conso_relay /usr/bin

uninstall:
	rm -f /usr/bin/inverter_conso_relay